node('master'){
    try{
        stage('Clonando o repositório'){
            checkout scm            

        }
        stage('Instalando o python' ){
             sh 'apt-get install python3-pip'
            
            
             
        }
        stage('Verificando a versão do python' ){
             sh 'pwd'
             sh 'python -v'
            
             
        }

        stage('Executando o script.py'){
            sh 'cd ..'
            sh 'pwd'
            sh 'python3 script.py'
        }
        

        
    }catch (exec)
    {
	    currentBuild.result = 'FAILURE'
        throw new Exception(exec)
        
    }finally{

      
       
    } 
}